package sorttiingalgorithms;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class SorttingAlgorithms {

  public static void main(String[] args) throws FileNotFoundException, IOException {
    InputReaderToDictionary inputReaderToDictionary = new InputReaderToDictionary();
    DictionaryShuffler dictionaryShuffler = new DictionaryShuffler();
    
    DictionaryInterface dictionary;
    long startTime;
    
    dictionary = dictionaryShuffler.shuffle(inputReaderToDictionary.read());
    startTime = System.currentTimeMillis();
    (new StringBubbleSort()).sort(dictionary);
    System.out.println((System.currentTimeMillis() - startTime));
    
    dictionary = dictionaryShuffler.shuffle(inputReaderToDictionary.read());
    startTime = System.currentTimeMillis();
    (new StringSelectionSort()).sort(dictionary);
    System.out.println((System.currentTimeMillis() - startTime));

    dictionary = dictionaryShuffler.shuffle(inputReaderToDictionary.read());
    startTime = System.currentTimeMillis();
    (new StringInsertionSort()).sort(dictionary);
    System.out.println((System.currentTimeMillis() - startTime));    
  /*
    dictionary = dictionaryShuffler.shuffle(new MockIntegerDictionary());
    startTime = System.currentTimeMillis();
    (new IntegerBubbleSort()).sort(dictionary);
    System.out.println("Integer Bubble Sort - time:" + (System.currentTimeMillis() - startTime));
*/
    
    dictionary = dictionaryShuffler.shuffle(inputReaderToDictionary.read());
    startTime = System.currentTimeMillis();
    Collections.sort(dictionary.getCollection());
    System.out.println((System.currentTimeMillis() - startTime));
  }
}
