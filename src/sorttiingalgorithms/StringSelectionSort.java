package sorttiingalgorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringSelectionSort implements SorttingAlgorithmInterface<String> {

  public DictionaryInterface sort(DictionaryInterface<String> dictionary) {
    List<String> collection = dictionary.getCollection();  
    
    for(Integer i = 0; i < collection.size(); i++) {
      Integer minIndex  = collection.indexOf(Collections.min(collection));
      
      for(Integer j = i; j < collection.size(); j++) {
         if (collection.get(minIndex).compareTo(collection.get(j)) < 0) {
           minIndex = j;
         }
      }
      Collections.swap(collection, i, minIndex);
    }

    DictionaryInterface dictionaryTmp = new StringDictionary();
    dictionaryTmp.setCollection(collection);
    
    return dictionaryTmp;
  }
}
