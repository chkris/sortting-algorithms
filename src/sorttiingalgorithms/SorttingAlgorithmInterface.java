package sorttiingalgorithms;

import java.util.ArrayList;

public interface SorttingAlgorithmInterface<T> {

  public DictionaryInterface sort(DictionaryInterface<T> dictionary);
}
