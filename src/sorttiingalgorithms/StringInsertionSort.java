package sorttiingalgorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringInsertionSort implements SorttingAlgorithmInterface<String> {

    public DictionaryInterface sort(DictionaryInterface<String> dictionary) {
        List<String> collection = dictionary.getCollection();  

        DictionaryInterface dictionaryTmp = new StringDictionary();
        dictionaryTmp.setCollection(insertSort(collection));

        return dictionaryTmp;
    }

    public List<String> insertSort(List<String> source){
        int index = 1;

        while (index < source.size()){
            insertSorted((String)(source.get(index)), source, index);
            index = index + 1;
        }
        return source;
    }	
	
    public List<String> insertSorted(String s, List<String> source, int index){
      int loc = index - 1;
      while ((loc >= 0) &&

      (s.compareTo((String)source.get(loc)) <= 0)){
        source.set(loc + 1, source.get(loc));
        loc = loc - 1;
      }
      source.set(loc+1, s);
      return source;
    }
}
