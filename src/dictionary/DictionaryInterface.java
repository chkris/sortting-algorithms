package dictionary;

import java.util.List;

public interface DictionaryInterface<T> {
  
  public List<T> getCollection();
 
  public void setCollection(List<T> collection);
}
