package dictionary;

import dictionary.DictionaryInterface;
import java.util.ArrayList;
import java.util.List;

public class IntegerDictionary implements DictionaryInterface<Integer>{

  protected List collection; 

  public IntegerDictionary() {
    collection = new ArrayList<Integer>();
  }
  
  public void add(Integer t) {
    collection.add(t);
  }
  
  public List<Integer> getCollection() {
    return collection;
  }

  public void setCollection(List<Integer> collection) {
    this.collection = collection;
  }
}
