package dictionary;

import java.util.ArrayList;
import java.util.List;

public class StringDictionary implements DictionaryInterface<String>{

  protected List collection; 

  public StringDictionary() {
    collection = new ArrayList<String>();
  }
  
  public void add(String t) {
    collection.add(t);
  }
  
  public List<String> getCollection() {
    return collection;
  }

  public void setCollection(List<String> collection) {
    this.collection = collection;
  }
}
