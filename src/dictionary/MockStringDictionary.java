package dictionary;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MockStringDictionary implements DictionaryInterface<String>{

  protected List collection; 

  public MockStringDictionary() {
    collection = new ArrayList<String>();
    for(int i=0; i < 1000000; i++) {
      collection.add(String.valueOf(i));
    }
  }
  
  public void add(String element) {
    collection.add(element);
  }
  
  public List<String> getCollection() {
    return collection;
  }

  public void setCollection(List<String> collection) {
    this.collection = collection;
  }
}
