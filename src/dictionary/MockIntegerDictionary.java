package dictionary;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MockIntegerDictionary implements DictionaryInterface<Integer>{

  protected List collection; 

  public MockIntegerDictionary() {
    collection = new ArrayList<Integer>();
    for(int i=0; i < 20; i++) {
      collection.add(i);
    }
  }
  
  public void add(Integer element) {
    collection.add(element);
  }
  
  public List<Integer> getCollection() {
    return collection;
  }

  public void setCollection(List<Integer> collection) {
    this.collection = collection;
  }
}
