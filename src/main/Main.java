package main;

import dictionary.DictionaryInterface;
import dictionary.MockIntegerDictionary;
import algorithms.shell.IntegerShellSort;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import shuffler.DictionaryShuffler;
import reader.InputReaderToDictionary;
import dictionary.MockStringDictionary;
import algorithms.quick.IntegerQuickSort;
import algorithms.quick.StringQuickSort;

public class Main {

  public static void main(String[] args) throws FileNotFoundException, IOException {
    InputReaderToDictionary inputReaderToDictionary = new InputReaderToDictionary();
    DictionaryShuffler dictionaryShuffler = new DictionaryShuffler();
    
    DictionaryInterface dictionary;
    long startTime;

    
    dictionary = dictionaryShuffler.shuffle(inputReaderToDictionary.read());
  
    startTime = System.currentTimeMillis();
    DictionaryInterface tmpDictionary = (new StringQuickSort()).sort(dictionary);
    
    System.out.println();
    for(Object x : tmpDictionary.getCollection()) {
        System.out.print(String.valueOf(x) + " ");
    }
    
     
    System.out.println("String Quick Sort - time:" + (System.currentTimeMillis() - startTime));
  }
  
  
}
