package shuffler;

import dictionary.DictionaryInterface;
import dictionary.StringDictionary;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DictionaryShuffler<T> {
  
  public DictionaryInterface shuffle(DictionaryInterface<T> dictionary) {
    DictionaryInterface dictionaryTmp = new StringDictionary();
    List collection = dictionary.getCollection();
    
    Collections.shuffle(collection);
    Collections.shuffle(collection);
    Collections.shuffle(collection);
    Collections.shuffle(collection);
    dictionaryTmp.setCollection(collection);
    
    return dictionaryTmp;    
  }
}
