package reader;

import dictionary.StringDictionary;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class InputReaderToDictionary {

  public final static int MAX_ITEMS = 100000000;  
    
  public StringDictionary read() throws FileNotFoundException, IOException {
    BufferedReader bufferedReader = new BufferedReader(new FileReader("polish.dic"));
    StringDictionary dictionary = new StringDictionary();
    Integer counter = 0;
    
    
    try {
      String line = bufferedReader.readLine();
      
      while(null != line && counter < MAX_ITEMS) {
        counter++;
        dictionary.add(line);
        line = bufferedReader.readLine();
      }
    } finally {
      bufferedReader.close();
    }
   
    return dictionary;
  }
}
