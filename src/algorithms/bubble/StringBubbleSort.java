package algorithms.bubble;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import dictionary.DictionaryInterface;
import algorithms.SorttingAlgorithmInterface;
import dictionary.StringDictionary;

public class StringBubbleSort implements SorttingAlgorithmInterface<String> {

  public DictionaryInterface sort(DictionaryInterface<String> dictionary) {
    List<String> collection = dictionary.getCollection();  
    
    for(Integer i = 0; i < collection.size(); i++) {
      for(Integer j = 1; j < collection.size(); j++) {
        if (collection.get(j - 1).compareTo(collection.get(j)) > 0) {
          Collections.swap(collection, j-1, j);
        }
      }
    }

    DictionaryInterface dictionaryTmp = new StringDictionary();
    dictionaryTmp.setCollection(collection);
    
    return dictionaryTmp;
  }
}
