package algorithms.bubble;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import dictionary.DictionaryInterface;
import dictionary.IntegerDictionary;
import algorithms.SorttingAlgorithmInterface;

public class IntegerBubbleSort implements SorttingAlgorithmInterface<Integer> {

  public DictionaryInterface sort(DictionaryInterface<Integer> dictionary) {
    List<Integer> collection = dictionary.getCollection();  
    Integer size = collection.size();
    
    for(Integer i = 0; i < size; i++) {
      for(Integer j = 1; j < size; j++) {
        if (collection.get(j - 1) < collection.get(j)) {
          Collections.swap(collection, j-1, j);
        }
      }
    }

    DictionaryInterface dictionaryTmp = new IntegerDictionary();
    dictionaryTmp.setCollection(collection);
    
    return dictionaryTmp;
  }
}
