package algorithms.quick;
import algorithms.shell.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import dictionary.DictionaryInterface;
import dictionary.IntegerDictionary;
import algorithms.SorttingAlgorithmInterface;

public class IntegerQuickSort implements SorttingAlgorithmInterface<Integer> {

  List<Integer> collection;  
    
  public DictionaryInterface sort(DictionaryInterface<Integer> dictionary) {
    this.collection = dictionary.getCollection();  
    Integer size = this.collection.size();
    
    int pivot = this.getMedian(size);
    
    quick(pivot, size-1);
    
    DictionaryInterface dictionaryTmp = new IntegerDictionary();
    dictionaryTmp.setCollection(this.collection);
    
    return dictionaryTmp;
  }
  
  protected void quick(int l, int r) {
      if (l < r) {
          Integer t = this.collection.get(l);
          Integer s = l;
          
          for (int i = l + 1; i <= r; i++) {
              if (this.collection.get(i) < t) {
                  s++;
                  Collections.swap(this.collection, s, i);
              }
          }
          Collections.swap(this.collection, l, s);
          quick(l, s - 1);
          quick(s + 1, r);
      }
  }
  
  protected int getMedian(int size)
  {      
      List<Integer> list = new ArrayList<Integer>();
      list.add(this.collection.get(0));
      list.add(this.collection.get(size - 1));
      list.add(this.collection.get(size/2));
      
      Collections.sort(list);
      
      return this.collection.indexOf(list.get(1));
  }
  
  protected int getMedianOfThree(int size)
  {      List<Integer> list = new ArrayList<Integer>();
     
      for(int i=1; i <= 3; i++) {
          list.add(this.collection.get(this.getMedian(size/3 * i)));
      }
      
      Collections.sort(list);
      
      return this.collection.indexOf(list.get(1)); 
  }
}
