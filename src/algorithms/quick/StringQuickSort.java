package algorithms.quick;
import algorithms.shell.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import dictionary.DictionaryInterface;
import dictionary.IntegerDictionary;
import algorithms.SorttingAlgorithmInterface;
import algorithms.insertion.StringInsertionSort;

public class StringQuickSort implements SorttingAlgorithmInterface<String> {

  List<String> collection;  
  public static final int LOW_PERFORMANCE_LIMIT = 26;  
  
  public DictionaryInterface sort(DictionaryInterface<String> dictionary) {
    this.collection = dictionary.getCollection();  
    Integer size = this.collection.size();
    
    if (size > LOW_PERFORMANCE_LIMIT) {
        int pivot = this.getMedian(size);
        quick(pivot, size-1);
            
        DictionaryInterface dictionaryTmp = new IntegerDictionary();
        dictionaryTmp.setCollection(this.collection);
    
        return dictionaryTmp;        
    } else {
        return (new StringInsertionSort()).sort(dictionary);
    }
  }
  
  protected void quick(int l, int r) {
      if (l < r) {
          String t = this.collection.get(l);
          Integer s = l;
          
          for (int i = l + 1; i <= r; i++) {
              if (this.collection.get(i).compareTo(t) < 0) {
                  s++;
                  Collections.swap(this.collection, s, i);
              }
          }
          Collections.swap(this.collection, l, s);
          quick(l, s - 1);
          quick(s + 1, r);
      }
  }
  
  protected int getMedian(int size)
  {      
      List<String> list = new ArrayList<String>();
      list.add(this.collection.get(0));
      list.add(this.collection.get(size - 1));
      list.add(this.collection.get(size/2));
      
      Collections.sort(list);
      
      return this.collection.indexOf(list.get(1));
  }
  
  protected int getMedianOfThree(int size)
  {      List<String> list = new ArrayList<String>();
     
      for(int i=1; i <= 3; i++) {
          list.add(this.collection.get(this.getMedian(size/3 * i)));
      }
      
      Collections.sort(list);
      
      return this.collection.indexOf(list.get(1)); 
  }
}
