package algorithms.shell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import dictionary.DictionaryInterface;
import dictionary.IntegerDictionary;
import algorithms.SorttingAlgorithmInterface;

public class IntegerShellSort implements SorttingAlgorithmInterface<Integer> {

  public DictionaryInterface sort(DictionaryInterface<Integer> dictionary) {
    List<Integer> collection = dictionary.getCollection();  
    Integer size = collection.size();
    int hSize = 4;
    
    int h = 1;
    while (h < size/(hSize*hSize)) {
        h = hSize*h + 1;
    }
    while (h > 0) {
        for (int i = h; i < size; i++) {
            Integer x = collection.get(i);
            Integer j = i;
            while (j >= h && x < collection.get(j - h)) {
                Collections.swap(collection, j, j - h);
                j -= h;
            }
            collection.set(j, x);
        }
        h /= hSize;
    }    
    
    DictionaryInterface dictionaryTmp = new IntegerDictionary();
    dictionaryTmp.setCollection(collection);
    
    return dictionaryTmp;
  }
}
