package algorithms.shell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import dictionary.DictionaryInterface;
import dictionary.IntegerDictionary;
import algorithms.SorttingAlgorithmInterface;

public class StringShellSort implements SorttingAlgorithmInterface<String> {

  public DictionaryInterface sort(DictionaryInterface<String> dictionary) {
    List<String> collection = dictionary.getCollection();  
    Integer size = collection.size();
    int hSize = 4;
    
    int h = 1;
    while (h < size/(hSize*hSize)) {
        h = hSize*h + 1;
    }
    while (h > 0) {
        for (int i = h; i < size; i++) {
            String x = collection.get(i);
            Integer j = i;
            while (j >= h && x.compareTo(collection.get(j - h)) < 0) {
                Collections.swap(collection, j, j - h);
                j -= h;
            }
            collection.set(j, x);
        }
        h /= hSize;
    }    
    
    DictionaryInterface dictionaryTmp = new IntegerDictionary();
    dictionaryTmp.setCollection(collection);
    
    return dictionaryTmp;
  }
}
